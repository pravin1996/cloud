[1] What is cloud ?
==>
    The practice of using a network of remote servers hosted on the Internet to store, manage, and process data, rather than a local server or a personal computer.
    
[2] Advantages of cloud.
==>
   1) Reduced IT costs
   2) Scalability
   3) Flexibility of work practices

[3] What is EC-2 ?
==> Virtual computing environments or Virtual machine , known as instances

[4] Type of cloud 
==> 1) private cloud
    2) public  cloud
    3) Hybride cloud
